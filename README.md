# Построение доменной модели

Необходимо разработать надежный и безопасный микросервис «iМагазин». 
Для разработки сервиса предложен набор трейтов, которые необходимо реализовать.

## ShopHttpController

Предоставляет единственный метод, через который "внешний мир" может
 взаимодействовать с сервисом. 
Взаимодействие
строится так: клиент отправляет в сервис операцию и набор параметров,
которые нужны для обработки данной операции. Если все с запросом хорошо,
сервис выполняет запрошенную операцию и возвращает соответствующий ответ.
Если что-то пошло не так, то возвращает сообщение об ошибке.

### Операции
* `register` — выполняет регистрацию Заказа. Параметры - список имен (`name`)
Товаров. Для успешной регистрации Заказа необходимо, что бы все Товары
существовали в БД сервиса. Ответ - идентификатор `order`

* `confirm` — подтверждает Заказ. Параметры — номер заказа `order` и способ доставки `delivery`.
 Остальные параметры зависят от выбранного способа.
 Ответ - суммарная стоимость заказа и доставки `price`.
 Формат адреса зависит от выбранного способа.
 Возможные комбинации
  * способ `почта` — `address` в виде строки `улица, дом-квартира`,
  * способ `курьер` — `телефон` в виде строки с номером телефона РФ,
  * способ `самовывоз` — `офис` в виде алфавитно-цифровой строки.
 
* `pay` — оплачивает Заказ. Параметры — `sum` с суммой оплаты в виде
 дробного числа с двумя знаками после запятой (например `123.45`), 
 десятичный разделитель — точка или запятая. 
 После оплаты сервис автоматически осуществляет доставку (мгновенно).
 Ответ — информация о доставки в параметре `info`

 
## Storage
Интерфейс БД, с которой работает магазин.
Умеет сохранять тройки строк «тип»-«ключ»-«значение», 
осуществлять поиск по типу и ключу,
находить все пары ключ-значение для заданного типа

### Известные типы
* `product` — товары
* `order` — заказы

## DeliveryBackend
Внешний сервис службы доставки, умеет доставлять почтой, курьером 
или самовывозом.
Метод `delivery` осуществляет доставку, метод `cost` — считает стоимость выбранного способа.
Набор параметров и возвращаемое значение зависит от выбранного метода.

### Методы доставки
* `Почта РФ` — принимает `улица`, `дом`, `квартира`, возвращает дату доставки
`дата` в формате `YYYY-MM-dd` в случае успешной доставки или причину отказа
в поле `причина` в случае неуспешной.
* `Курьер` — принимает номер телефона в виде 10 цифр в параметре
`номер телефона`, возвращает ФИО получателя в поле `фио` в случае успешной
доставки или отказ в поле `причина` в случае неуспешной.
* `Самовывоз` — `офис`, возвращает ФИО получателя в поле `фио` и
 паспорт в `паспорт` в случае успешной доставки или отказ в 
 поле `причина` в случае неуспешной.

### Расчет стоимости
Входные параметры - в точности такие же, как в Методах, возвращаемое значение - стоимость
в виде числа с двумя знаками после запятой в параметре `sum`.

## Работа с сервисом.
Работа с iМагазином строится следующим образом: 
* Клиент передает в Сервис список товаров, которые желает приобрести. 
* Сервис проверят запрос, проверяет наличие необходимых товаров и если все ОК,
регистрирует Заказ, в ответ возвращает его номер.
* Клиент подтверждает Заказа и передает параметры доставки
* Магазин определяет полную стоимость Заказа и сообщает её Клиенту
* Клиент осуществляет оплату полной стоимости
* Магазин проводит доставку Заказа выбранным способом.

## Требования к реализации
Сервис должен быть надежным. По этим понимается, гарантированный ответ сервиса в рамках протокола
в любом случае, возврат "сломанной фьючи" недопустим.
Сервис должен обеспечивать идемпотентность подтверждения и оплаты.
Разработанная модель данных и внутренних сервисов должны максимально четко описывать предложенный сценарий
 работы магазина и обеспечивать максимальный возможный контроль ошибок силами компилятора.

Необходимо реализовать как минимум `ShopHttpController` и `ShopApp.create`
Вся остальная внутренняя организация классов, трейтов и т.д. — на совести реализующего. 

