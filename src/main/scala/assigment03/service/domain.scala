package assigment03.service

import java.time.LocalDate

case class Money(sum: Long) extends AnyVal

case class OrderId(id: String) extends AnyVal

case class ItemName(name: String) extends AnyVal

case class Item(name: ItemName, price: Money)


sealed trait Delivery

object Delivery {

  case class Street(value: String) extends AnyVal
  case class House(value: String) extends AnyVal
  case class Apart(value: String) extends AnyVal

  case class Post(
                 street: Street,
                 house: House,
                 apart: Apart
                 ) extends Delivery

  case class Phone(value: String) extends AnyVal

  case class Courier(phone: Phone) extends Delivery

  case class Office(value: String) extends AnyVal

  case class Self(office: Office) extends Delivery
}

sealed trait DeliveryInfo{
  val isSuccess: Boolean
}

object DeliveryInfo{
  case class DeliveryFailed(msg: String) extends DeliveryInfo{
    override val isSuccess: Boolean = false
  }

  case class PostSuccess(date: LocalDate) extends DeliveryInfo{
    override val isSuccess: Boolean = true
  }

  case class CourierSuccess(fio: String) extends DeliveryInfo{
    override val isSuccess: Boolean = true
  }

  case class SelfSuccess(fio: String, passport: String) extends DeliveryInfo{
    override val isSuccess: Boolean = true
  }
}

sealed trait Order{
  def id: OrderId
  def itemsPrice: Money
}

object Order {

  case class RegisteredOrder(
                              id: OrderId,
                              items: List[Item],
                              itemsPrice: Money
                            ) extends Order


  case class ConfirmedOrder(
                            id: OrderId,
                            items: List[Item],
                            itemsPrice: Money,
                            delivery: Delivery,
                            fullPrice: Money
                          ) extends Order

  case class PayedOrder(
                         id: OrderId,
                         items: List[Item],
                         itemsPrice: Money,
                         delivery: Delivery,
                         fullPrice: Money
                       ) extends Order

  case class DeliveredOrder(
                         id: OrderId,
                         items: List[Item],
                         itemsPrice: Money,
                         delivery: Delivery,
                         fullPrice: Money,
                         deliveryInfo: DeliveryInfo
                       ) extends Order
}