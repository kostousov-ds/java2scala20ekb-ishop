package assigment03.service

import assigment03.repository.Storage

import scala.concurrent.{ExecutionContext, Future}

trait StorageRepository {

  def findItems(items: List[ItemName]): Future[List[Item]]

  def saveOrder(order: Order): Future[Unit]

  def findOrder(orderId: OrderId): Future[Option[Order]]

}

class StorageRepositoryImpl(
                             val storage: Storage,
                             implicit val _ex: ExecutionContext
                           ) extends StorageRepository {

  import StorageRepositoryImpl._

  override def findItems(items: List[ItemName]): Future[List[Item]] =
    Future.traverse(items) { name =>
      storage.find(types.ITEM, name.name).map {_.map { data =>
          Item(name, Money(data.toLong))
      }}
    }.map(_.flatten)

  override def saveOrder(order: Order): Future[Unit] = ???

  override def findOrder(orderId: OrderId): Future[Option[Order]] = ???

/*

{
  "_type": "RegisteredOrder",
  "id": "123456",
}


*/

}

object StorageRepositoryImpl{
  object types {
    val ITEM = "item"
    val ORDER = "order"
  }
}