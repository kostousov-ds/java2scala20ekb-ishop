package assigment03.controller

import assigment03.controller.ShopHttpController.Response
import assigment03.service.Delivery.{Apart, House, Post, Street}
import assigment03.service.{Delivery, ItemName, OrderId, ShopService}
import com.sun.nio.sctp.AbstractNotificationHandler

import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

trait ShopHttpController {
  def handle(operation: String, parameters: Map[String, List[String]]): Future[Response]
}

object ShopHttpController {

  sealed trait Response

  object Response {

    case class Success(parameters: Map[String, String]) extends Response

    case class Error(message: String) extends Response

  }

}

class ShopHttpControllerImpl(
                            service: ShopService,
                            implicit val _ec: ExecutionContext
                            ) extends ShopHttpController{
  override def handle(
                       operation: String,
                       parameters: Map[String, List[String]]): Future[Response] = { operation match {
    case "register" => doRegister(parameters)
    case "confirm" => ???
    case "pay" => ???
    case _ => ???
  }}.recover{
    case e: Exception => Response.Error(e.getMessage)
  }

  def doRegister(parameters: Map[String, List[String]]): Future[Response] =
    parameters.get("name").fold(Future.failed[Response](new Exception("Parameter 'name' was not found"))){
      names => service
        .register(names.map(ItemName))
        .map(id => Response.Success(Map("order" -> id.id)))
    }

  def doConfirm(parameters: Map[String, List[String]]): Future[Response] = {
    for {
      orderId <- parameters.get("order").flatMap(_.headOption).map(OrderId)
      // delivery <- parameters.extract[Delivery]()
      delivery <- parseDelivery(parameters)
    } yield service.confirm(orderId, delivery)
  }.getOrElse(Future.failed(new Exception("Invalid parameters")))
    .map(money => Response.Success(Map("price" -> s"${money.sum}")))

  def parseDelivery(parameters: Map[String, List[String]]):Option[Delivery] = for {
    deliveryStr <- parameters.get("delivery").flatMap(_.headOption)
    delivery <- deliveryStr match {
      case "почта" => parseDeliveryPost(parameters)
      case "курьер" => ???
      case "самовывоз" => ???
    }
  } yield delivery

  val postRegExp: Regex = "(\\w+), (\\w+)-(\\w+)".r

  def parseDeliveryPost(parameters: Map[String, List[String]]): Option[Post] = for {
    addressStr <- parameters.get("address").flatMap(_.headOption)
    ret <- addressStr match {
      case postRegExp(street, house, apart) => Some(Post(Street(street), House(house), Apart(apart)))
      case _ => None
    }
  } yield ret
}