package assigment03.backend

import scala.concurrent.Future

trait DeliveryBackend {
  def delivery(method: String, parameters: Map[String, String]): Future[Map[String, String]]
  def cost(method: String, parameters: Map[String, String]): Future[Map[String, String]]
}

