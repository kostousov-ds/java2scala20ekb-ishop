package assigment03.repository

import scala.concurrent.Future

trait Storage {
  def save(`type`: String, key: String, data: String): Future[String]

  def find(`type`: String, key: String): Future[Option[String]]

  def all(`type`: String): Future[Map[String, String]]
}
