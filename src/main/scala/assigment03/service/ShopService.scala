package assigment03.service

import assigment03.service.DeliveryInfo.DeliveryFailed
import assigment03.service.Order.{ConfirmedOrder, DeliveredOrder, RegisteredOrder}

import scala.concurrent.{ExecutionContext, Future}

trait ShopService {
  def register(items: List[ItemName]): Future[OrderId]

  def confirm(orderId: OrderId, delivery: Delivery): Future[Money]

  def pay(orderId: OrderId, sum: Money): Future[DeliveryInfo]
}

trait DeliveryRepository {
  def delivery(delivery: Delivery): Future[DeliveryInfo]

  def price(delivery: Delivery): Future[Money]
}

class ShopServiceImpl(
                     val idGenerator: () => OrderId,
                     val storage: StorageRepository,
                     val deliveryRepository: DeliveryRepository,
                     implicit val _ec: ExecutionContext
                     ) extends ShopService {


  override def register(items: List[ItemName]): Future[OrderId] =
    for {
      foundItems <- storage.findItems(items)
      _ <- if (isAllFound(items, foundItems)) Future.successful(())
      else
        Future.failed(new Exception("Some items was not found"))
      order = createOrder(foundItems)
      _ <- storage.saveOrder(order)
    } yield order.id

  def createOrder(items: List[Item]): Order = {
    val id: OrderId = idGenerator()
    //TODO научиться суммировать Price
    val price = items.map(_.price.sum).sum
    Order.RegisteredOrder(id, items,Money(price))
  }


  def isAllFound(required: List[ItemName], found: List[Item]): Boolean =
    required.size == found.size

  override def confirm(orderId: OrderId, delivery: Delivery): Future[Money] = for {
    orderOpt <- storage.findOrder(orderId)
    order = orderOpt.getOrElse(throw new Exception(s"Order '$orderId' was not found'"))
    registeredOrder <- checkRegisteredOrder(order)
    price <- deliveryRepository.price(delivery)
    confirmedOrder = confirmOrder(registeredOrder, delivery, price)
    _ <- storage.saveOrder(confirmedOrder)
  } yield confirmedOrder.fullPrice

  def checkRegisteredOrder: Order => Future[RegisteredOrder] = {
    case order: RegisteredOrder => Future.successful(order)
    case _ => Future.failed(new Exception("Invalid order state"))
  }

  def confirmOrder(order: RegisteredOrder, delivery: Delivery, deliveryPrice: Money): ConfirmedOrder =
    ConfirmedOrder(
      id = order.id,
      items = order.items,
      itemsPrice = order.itemsPrice,
      delivery = delivery,
      fullPrice = Money(order.itemsPrice.sum + deliveryPrice.sum)
    )

  override def pay(orderId: OrderId, sum: Money): Future[DeliveryInfo] = for {
    orderOpt <- storage.findOrder(orderId)
    order = orderOpt.getOrElse(throw new Exception(s"Order '$orderId' was not found'"))
    confirmedOrder <- checkConfirmedOrder(order)
    _ <- checkPayedSum(confirmedOrder, sum)
    info <- deliveryRepository.delivery(confirmedOrder.delivery)
    deliveredOrder <- info match {
      case DeliveryFailed(msg) => Future.failed(new Exception(s"Can't delivery order: $msg"))
      case other: DeliveryInfo => Future.successful(createDeliveredOrder(confirmedOrder, other))
    }
    _ <- storage.saveOrder(deliveredOrder)
  } yield deliveredOrder.deliveryInfo

  def checkConfirmedOrder: Order => Future[ConfirmedOrder] = {
    case order: ConfirmedOrder => Future.successful(order)
    case _ => Future.failed(new Exception("Invalid order state"))
  }

  def checkPayedSum(order:ConfirmedOrder, payed: Money): Future[Unit] =
    if(order.fullPrice.sum == payed.sum)
      Future.successful(())
    else
      Future.failed(new Exception("Wrong sum"))

  private[service] def createDeliveredOrder(order: ConfirmedOrder, info: DeliveryInfo): DeliveredOrder =
    DeliveredOrder(
      id = order.id,
      items = order.items,
      itemsPrice = order.itemsPrice,
      delivery = order.delivery,
      fullPrice = order.fullPrice,
      deliveryInfo = info
    )
}